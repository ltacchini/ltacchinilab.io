View site: https://ltacchini.gitlab.io

-----
# HomePage



## Configurations

- Jekyll (last version)
- Default `jekyll build`

- GitLab-CI config: [`.gitlab-ci.yml`](https://gitlab.com/jekyll-themes/grayscale/blob/master/.gitlab-ci.yml)
- Gitignore: [`.gitignore`](https://gitlab.com/jekyll-themes/grayscale/blob/master/.gitignore)
- Jekyll config: [`_config.yml`](https://gitlab.com/jekyll-themes/grayscale/blob/master/_config.yml)
- Licence: [`Apache`](https://gitlab.com/jekyll-themes/grayscale/blob/master/LICENCE)